namespace forum.net.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Forum : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Categories", "ParentID", "dbo.Categories");
            DropIndex("dbo.Categories", new[] { "ParentID" });
            CreateTable(
                "dbo.Fora",
                c => new
                    {
                        ForumID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Sort = c.Int(nullable: false),
                        Category_CategoryID = c.Int(),
                    })
                .PrimaryKey(t => t.ForumID)
                .ForeignKey("dbo.Categories", t => t.Category_CategoryID)
                .Index(t => t.Category_CategoryID);
            
            DropColumn("dbo.Categories", "ParentID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Categories", "ParentID", c => c.Int(nullable: false));
            DropForeignKey("dbo.Fora", "Category_CategoryID", "dbo.Categories");
            DropIndex("dbo.Fora", new[] { "Category_CategoryID" });
            DropTable("dbo.Fora");
            CreateIndex("dbo.Categories", "ParentID");
            AddForeignKey("dbo.Categories", "ParentID", "dbo.Categories", "CategoryId");
        }
    }
}
