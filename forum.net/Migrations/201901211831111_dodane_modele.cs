namespace forum.net.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dodane_modele : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Threads",
                c => new
                    {
                        ThreadId = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 35),
                        ThreadtypeId = c.Int(nullable: false),
                        CreatedById = c.String(maxLength: 128),
                        UpdatedById = c.String(maxLength: 128),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ThreadId)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedById)
                .ForeignKey("dbo.Threadtypes", t => t.ThreadtypeId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedById)
                .Index(t => t.ThreadtypeId)
                .Index(t => t.CreatedById)
                .Index(t => t.UpdatedById);
            
            CreateTable(
                "dbo.Bans",
                c => new
                    {
                        BanId = c.Int(nullable: false, identity: true),
                        Reason = c.String(nullable: false, maxLength: 255),
                        Duration = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128),
                        CreatedById = c.String(maxLength: 128),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.BanId)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedById)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.UserId)
                .Index(t => t.CreatedById)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.Complains",
                c => new
                    {
                        ComplainId = c.Int(nullable: false, identity: true),
                        Reason = c.String(nullable: false, maxLength: 255),
                        CreatedAt = c.DateTime(nullable: false),
                        PostId = c.Int(nullable: false),
                        CreatedById = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ComplainId)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedById)
                .ForeignKey("dbo.Posts", t => t.PostId, cascadeDelete: true)
                .Index(t => t.PostId)
                .Index(t => t.CreatedById);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        PostId = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false, maxLength: 255),
                        ThreadId = c.Int(nullable: false),
                        CreatedById = c.String(maxLength: 128),
                        UpdatedById = c.String(maxLength: 128),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.PostId)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedById)
                .ForeignKey("dbo.Threads", t => t.ThreadId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedById)
                .Index(t => t.ThreadId)
                .Index(t => t.CreatedById)
                .Index(t => t.UpdatedById);
            
            CreateTable(
                "dbo.Threadtypes",
                c => new
                    {
                        ThreadtypeId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 35),
                    })
                .PrimaryKey(t => t.ThreadtypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Threads", "UpdatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.Threads", "ThreadtypeId", "dbo.Threadtypes");
            DropForeignKey("dbo.Threads", "CreatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.Complains", "PostId", "dbo.Posts");
            DropForeignKey("dbo.Posts", "UpdatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.Posts", "ThreadId", "dbo.Threads");
            DropForeignKey("dbo.Posts", "CreatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.Complains", "CreatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.Bans", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Bans", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Bans", "CreatedById", "dbo.AspNetUsers");
            DropIndex("dbo.Posts", new[] { "UpdatedById" });
            DropIndex("dbo.Posts", new[] { "CreatedById" });
            DropIndex("dbo.Posts", new[] { "ThreadId" });
            DropIndex("dbo.Complains", new[] { "CreatedById" });
            DropIndex("dbo.Complains", new[] { "PostId" });
            DropIndex("dbo.Bans", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.Bans", new[] { "CreatedById" });
            DropIndex("dbo.Bans", new[] { "UserId" });
            DropIndex("dbo.Threads", new[] { "UpdatedById" });
            DropIndex("dbo.Threads", new[] { "CreatedById" });
            DropIndex("dbo.Threads", new[] { "ThreadtypeId" });
            DropTable("dbo.Threadtypes");
            DropTable("dbo.Posts");
            DropTable("dbo.Complains");
            DropTable("dbo.Bans");
            DropTable("dbo.Threads");
        }
    }
}
