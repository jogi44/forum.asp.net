namespace forum.net.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserApp : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Fora", "Discriminator");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Fora", "Discriminator", c => c.String(nullable: false, maxLength: 128));
        }
    }
}
