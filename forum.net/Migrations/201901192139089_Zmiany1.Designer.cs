// <auto-generated />
namespace forum.net.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Zmiany1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Zmiany1));
        
        string IMigrationMetadata.Id
        {
            get { return "201901192139089_Zmiany1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
