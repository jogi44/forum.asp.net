namespace forum.net.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dodanie_forumid_thread : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Threads", "ForumId", c => c.Int(nullable: false));
            CreateIndex("dbo.Threads", "ForumId");
            AddForeignKey("dbo.Threads", "ForumId", "dbo.Fora", "ForumId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Threads", "ForumId", "dbo.Fora");
            DropIndex("dbo.Threads", new[] { "ForumId" });
            DropColumn("dbo.Threads", "ForumId");
        }
    }
}
