using forum.net.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace forum.net.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<forum.net.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(forum.net.Models.ApplicationDbContext context)
        {
            IdentityManager im = new IdentityManager();

            // Tworzenie rang
            if (!im.RoleExists("Administrator")) im.CreateRole("Administrator");
            if (!im.RoleExists("Moderator")) im.CreateRole("Moderator");
            if (!im.RoleExists("User")) im.CreateRole("User");

            // Tworzenie użytkownika
            if (!context.Users.Any(u => u.UserName == "admin@wp.pl"))
            {
                var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var user = new ApplicationUser
                {
                    UserName = "admin@wp.pl",
                    Email = "admin@wp.pl",
                    Nickname = "Admin",
                    CreatedAt = DateTime.Now
                };

                manager.Create(user, "test123");
                manager.AddToRole(user.Id, "User");
                manager.AddToRole(user.Id, "Administrator");
            }

        }
    }
}
