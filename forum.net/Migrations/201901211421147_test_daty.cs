namespace forum.net.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test_daty : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Categories", "CreatedAt", c => c.DateTime());
            AlterColumn("dbo.Categories", "UpdatedAt", c => c.DateTime());
            AlterColumn("dbo.Fora", "CreatedAt", c => c.DateTime());
            AlterColumn("dbo.Fora", "UpdatedAt", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Fora", "UpdatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Fora", "CreatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Categories", "UpdatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Categories", "CreatedAt", c => c.DateTime(nullable: false));
        }
    }
}
