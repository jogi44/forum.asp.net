namespace forum.net.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class forum_zmiany : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Fora", "Description", c => c.String(nullable: false, maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Fora", "Description");
        }
    }
}
