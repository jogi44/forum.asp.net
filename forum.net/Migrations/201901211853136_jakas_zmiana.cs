namespace forum.net.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class jakas_zmiana : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Adverts",
                c => new
                    {
                        AdvertId = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 35),
                        Body = c.String(nullable: false, maxLength: 255),
                        CreatedById = c.String(maxLength: 128),
                        UpdatedById = c.String(maxLength: 128),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.AdvertId)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedById)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedById)
                .Index(t => t.CreatedById)
                .Index(t => t.UpdatedById);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        MessageId = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 35),
                        Body = c.String(nullable: false, maxLength: 255),
                        CreatedAt = c.DateTime(nullable: false),
                        ToUserId = c.String(maxLength: 128),
                        CreatedById = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.MessageId)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedById)
                .ForeignKey("dbo.AspNetUsers", t => t.ToUserId)
                .Index(t => t.ToUserId)
                .Index(t => t.CreatedById);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Messages", "ToUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Messages", "CreatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.Adverts", "UpdatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.Adverts", "CreatedById", "dbo.AspNetUsers");
            DropIndex("dbo.Messages", new[] { "CreatedById" });
            DropIndex("dbo.Messages", new[] { "ToUserId" });
            DropIndex("dbo.Adverts", new[] { "UpdatedById" });
            DropIndex("dbo.Adverts", new[] { "CreatedById" });
            DropTable("dbo.Messages");
            DropTable("dbo.Adverts");
        }
    }
}
