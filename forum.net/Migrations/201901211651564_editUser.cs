namespace forum.net.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editUser : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "UpdatedAt");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "UpdatedAt", c => c.DateTime(nullable: false));
        }
    }
}
