namespace forum.net.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Zmiany2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Fora", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Categories", "Name", c => c.String(nullable: false, maxLength: 55));
            AlterColumn("dbo.Fora", "Name", c => c.String(nullable: false, maxLength: 15));
            AlterColumn("dbo.AspNetUsers", "Nickname", c => c.String(nullable: false, maxLength: 25));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "Nickname", c => c.String(maxLength: 31));
            AlterColumn("dbo.Fora", "Name", c => c.String());
            AlterColumn("dbo.Categories", "Name", c => c.String(maxLength: 55));
            DropColumn("dbo.Fora", "Discriminator");
        }
    }
}
