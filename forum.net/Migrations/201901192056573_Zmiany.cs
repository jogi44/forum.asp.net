namespace forum.net.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Zmiany : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Fora", "CategoryID_CategoryID", "dbo.Categories");
            DropIndex("dbo.Fora", new[] { "CategoryID_CategoryID" });
            RenameColumn(table: "dbo.Fora", name: "CategoryID_CategoryID", newName: "CategoryId");
            AlterColumn("dbo.Fora", "CategoryId", c => c.Int(nullable: false));
            CreateIndex("dbo.Fora", "CategoryId");
            AddForeignKey("dbo.Fora", "CategoryId", "dbo.Categories", "CategoryId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Fora", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Fora", new[] { "CategoryId" });
            AlterColumn("dbo.Fora", "CategoryId", c => c.Int());
            RenameColumn(table: "dbo.Fora", name: "CategoryId", newName: "CategoryID_CategoryID");
            CreateIndex("dbo.Fora", "CategoryID_CategoryID");
            AddForeignKey("dbo.Fora", "CategoryID_CategoryID", "dbo.Categories", "CategoryID");
        }
    }
}
