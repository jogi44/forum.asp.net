namespace forum.net.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Zmiany1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Fora", "CreatedAt", c => c.DateTime(nullable: false));
            AddColumn("dbo.Fora", "UpdatedAt", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Fora", "UpdatedAt");
            DropColumn("dbo.Fora", "CreatedAt");
        }
    }
}
