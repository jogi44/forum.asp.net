namespace forum.net.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ZmianaCategory : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Fora", name: "Category_CategoryID", newName: "CategoryID_CategoryID");
            RenameIndex(table: "dbo.Fora", name: "IX_Category_CategoryID", newName: "IX_CategoryID_CategoryID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Fora", name: "IX_CategoryID_CategoryID", newName: "IX_Category_CategoryID");
            RenameColumn(table: "dbo.Fora", name: "CategoryID_CategoryID", newName: "Category_CategoryID");
        }
    }
}
