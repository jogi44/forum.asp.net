﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using forum.net.Models;

namespace forum.net.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ThreadtypesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Threadtypes
        public ActionResult Index()
        {
            return View(db.Threadtypes.ToList());
        }

        // GET: Threadtypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Threadtype threadtype = db.Threadtypes.Find(id);
            if (threadtype == null)
            {
                return HttpNotFound();
            }
            return View(threadtype);
        }

        // GET: Threadtypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Threadtypes/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ThreadtypeId,Name")] Threadtype threadtype)
        {
            if (ModelState.IsValid)
            {
                db.Threadtypes.Add(threadtype);
                db.SaveChanges();
                return RedirectToAction("Admin", "Home");
            }

            return View(threadtype);
        }

        // GET: Threadtypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Threadtype threadtype = db.Threadtypes.Find(id);
            if (threadtype == null)
            {
                return HttpNotFound();
            }
            return View(threadtype);
        }

        // POST: Threadtypes/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ThreadtypeId,Name")] Threadtype threadtype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(threadtype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Admin", "Home");
            }
            return View(threadtype);
        }

        // GET: Threadtypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Threadtype threadtype = db.Threadtypes.Find(id);
            if (threadtype == null)
            {
                return HttpNotFound();
            }
            return View(threadtype);
        }

        // POST: Threadtypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Threadtype threadtype = db.Threadtypes.Find(id);
            db.Threadtypes.Remove(threadtype);
            db.SaveChanges();
            return RedirectToAction("Admin", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
