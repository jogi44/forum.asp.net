﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using forum.net.Models;
using Microsoft.AspNet.Identity;

namespace forum.net.Controllers
{
    
    public class ThreadsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Threads(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var threads = db.Threads.Where(t => t.ForumId == id).Include(t => t.CreatedBy).OrderByDescending(t => t.ThreadtypeId).ToList();
            var forum = db.Fora.Find(id);
            return View("~/Views/Home/Threads.cshtml", new ThreadsViewModel { Threads = threads, Forum = forum });
        }

        // GET: Threads
        public ActionResult Index()
        {
            var threads = db.Threads.Include(t => t.Threadtype);
            return View(threads.ToList());
        }

        // GET: Threads/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var posts = db.Posts.Where(t => t.ThreadId == id).Include(t => t.CreatedBy).ToList();
            Thread thread = db.Threads.Find(id);
            thread.Posts = posts;
            Threadtype threadtype = db.Threadtypes.Find(thread.ThreadtypeId);
            ViewBag.threadname = threadtype.Name;
            ViewBag.ThreadId = thread.ThreadId;
            if (thread == null)
            {
                return HttpNotFound();
            }
            return View(thread);
        }

        // GET: Threads/Create
        [Authorize]
        public ActionResult Create(int? forumId)
        {
            if (forumId == null || db.Fora.Find(forumId) == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var forum = db.Fora.Find(forumId);
            var threadtypes = new SelectList(db.Threadtypes, "ThreadtypeId", "Name");
            Thread thread = new Thread();
            thread.ForumId = (int)forumId;
            return View(new CreateThreadViewModel { Forum = forum, Threadtypes = threadtypes, Thread = thread});
        }

        // POST: Threads/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ThreadId,Title,ThreadtypeId,ForumId")] Thread thread, string post_body, int forum_id)
        {
            if (post_body.Length > 255)
                return RedirectToAction("Threads", "Threads", new { id = forum_id });

            if (ModelState.IsValid)
            {
                thread.ForumId = forum_id;
                thread.CreatedById = User.Identity.GetUserId();
                thread.UpdatedById = User.Identity.GetUserId();

                // Tworzenie pierwszego posta w nowym temacie
                Post newPost = new Post();
                newPost.Body = post_body;
                newPost.ThreadId = thread.ThreadId;
                newPost.CreatedById = User.Identity.GetUserId();
                newPost.UpdatedById = User.Identity.GetUserId();

                db.Threads.Add(thread);
                db.Posts.Add(newPost);
                db.SaveChanges();
                return RedirectToAction("Threads", "Threads", new { id = thread.ForumId });
            }

            var forum = db.Fora.Find(thread.ForumId);
            var threadtypes = new SelectList(db.Threadtypes, "ThreadtypeId", "Name");

            return View(new CreateThreadViewModel { Forum = forum, Threadtypes = threadtypes, Thread = thread });
        }

        // GET: Threads/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Thread thread = db.Threads.Find(id);
            if (thread == null)
            {
                return HttpNotFound();
            }
            ViewBag.ThreadtypeId = new SelectList(db.Threadtypes, "ThreadtypeId", "Name", thread.ThreadtypeId);
            return View(thread);
        }

        // POST: Threads/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ThreadId,Title,ThreadtypeId,CreatedById,UpdatedById,CreatedAt,UpdatedAt")] Thread thread)
        {
            if (ModelState.IsValid)
            {
                db.Entry(thread).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ThreadtypeId = new SelectList(db.Threadtypes, "ThreadtypeId", "Name", thread.ThreadtypeId);
            return View(thread);
        }

        // GET: Threads/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Thread thread = db.Threads.Find(id);
            if (thread == null)
            {
                return HttpNotFound();
            }
            return View(thread);
        }

        // POST: Threads/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Thread thread = db.Threads.Find(id);
            db.Threads.Remove(thread);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
