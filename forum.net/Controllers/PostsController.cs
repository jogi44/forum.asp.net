﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using forum.net.Models;
using Microsoft.AspNet.Identity;

namespace forum.net.Controllers
{
    
    public class PostsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Posts
        public ActionResult Index()
        {
            var posts = db.Posts.Include(p => p.CreatedBy).Include(p => p.Thread).Include(p => p.UpdatedBy);
            return View(posts.ToList());
        }

        // GET: Posts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // GET: Posts/Create
        [Authorize]
        public ActionResult Create(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var threads = db.Threads.Where(t => t.ForumId == id).Include(t => t.CreatedBy).ToList();
            var thread = db.Threads.Find(id);
            Post post = new Post();
            post.ThreadId = (int)id;

            return View(new Post { Thread = thread });
        }

        // POST: Posts/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PostId,Body,ThreadId,CreatedById,UpdatedById,CreatedAt,UpdatedAt")] Post post, int id)
        {
            if (ModelState.IsValid)
            {

                post.ThreadId = db.Threads.Find(id).ThreadId;
                post.CreatedById = User.Identity.GetUserId();
                post.UpdatedById = User.Identity.GetUserId();
                post.CreatedAt = DateTime.Now;
                post.UpdatedAt = DateTime.Now;
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Details", "Threads", new { id = post.ThreadId });
            }

            ViewBag.CreatedById = new SelectList(db.Users, "Id", "Nickname", post.CreatedById);
            ViewBag.ThreadId = new SelectList(db.Threads, "ThreadId", "Title", post.ThreadId);
            ViewBag.UpdatedById = new SelectList(db.Users, "Id", "Nickname", post.UpdatedById);
            return View(post);
        }

        // GET: Posts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.CreatedById = new SelectList(db.Users, "Id", "Nickname", post.CreatedById);
            ViewBag.ThreadId = new SelectList(db.Threads, "ThreadId", "Title", post.ThreadId);
            ViewBag.UpdatedById = new SelectList(db.Users, "Id", "Nickname", post.UpdatedById);
            return View(post);
        }

        // POST: Posts/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PostId,Body,ThreadId,CreatedById,UpdatedById,CreatedAt,UpdatedAt")] Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CreatedById = new SelectList(db.Users, "Id", "Nickname", post.CreatedById);
            ViewBag.ThreadId = new SelectList(db.Threads, "ThreadId", "Title", post.ThreadId);
            ViewBag.UpdatedById = new SelectList(db.Users, "Id", "Nickname", post.UpdatedById);
            return View(post);
        }

        // GET: Posts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
