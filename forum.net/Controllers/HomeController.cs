﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using forum.net.Models;

namespace forum.net.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View("Index", new HomeIndexViewModel());
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Admin()
        {

            return View("AdminPanel", new AdminPanelViewModel());
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}