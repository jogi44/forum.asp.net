﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace forum.net.Models
{
    public class Threadtype
    {
        public int ThreadtypeId { get; set; }

        [Required]
        [StringLength(35)]
        [DisplayName("Nazwa")]
        public string Name { get; set; }

        public virtual ICollection<Thread> Threads { get; set; }
    }
}