﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace forum.net.Models
{
    public class Complain
    {
        public int ComplainId { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayName("Powód")]
        public string Reason { get; set; }
        public DateTime CreatedAt { get; set; }

        public int PostId { get; set; }

        [ForeignKey("CreatedBy")]
        public string CreatedById { get; set; }

        public Post Post { get; set; }
        public ApplicationUser CreatedBy { get; set; }
    }
}