﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace forum.net.Models
{
    public class Thread : BaseEntity
    {
        public int ThreadId { get; set; }

        [Required]
        [StringLength(35)]
        [DisplayName("Tytuł")]
        public string Title { get; set; }

        [DisplayName("Forum")]
        public int ForumId { get; set; }

        [DisplayName("Typ wątku")]
        public int ThreadtypeId { get; set; }

        [ForeignKey("CreatedBy")]
        public string CreatedById { get; set; }

        [ForeignKey("UpdatedBy")]
        public string UpdatedById { get; set; }

        public Threadtype Threadtype { get; set; }
        public Forum Forum { get; set; }
        public ApplicationUser CreatedBy { get; set; }
        public ApplicationUser UpdatedBy { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }

    public class ThreadsViewModel
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public Forum Forum { get; set; }
        public ICollection<Thread> Threads { get; set; }
        public ICollection<Post> Posts { get; set; }
        public ICollection<Advert> Adverts => db.Adverts.ToList();
    }

    public class CreateThreadViewModel
    {
        public Thread Thread { get; set; }
        public Forum Forum { get; set; }
        public IEnumerable<SelectListItem> Threadtypes { get; set; }
    }
}