﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace forum.net.Models
{
    public class Ban
    {
        public int BanId { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayName("Powód")]
        public string Reason { get; set; }

        [Required]
        [DisplayName("Długość")]
        public int Duration { get; set; }

        public string UserId { get; set; }

        [ForeignKey("CreatedBy")]
        public string CreatedById { get; set; }

        public ApplicationUser User { get; set; }
        public ApplicationUser CreatedBy { get; set; }
    }
}