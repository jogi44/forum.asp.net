﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace forum.net.Models
{
    public class HomeIndexViewModel
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ICollection<Category> Categories => db.Categories.ToList();
        public ICollection<Forum> Forums => db.Fora.ToList();
        public ICollection<Advert> Adverts => db.Adverts.ToList();
        public ICollection<ApplicationUser> Users => db.Users.ToList();
        public ICollection<Thread> Threads => db.Threads.ToList();
        public ICollection<Post> Posts => db.Posts.ToList();

        public Advert Advert { get; set; }

        public Category Category { get; set; }
        public Forum Forum { get; set; }

    }
}