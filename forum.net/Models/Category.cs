﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace forum.net.Models
{
    public class Category : BaseEntity
    {
        public int CategoryId { get; set; }

        [Required]
        [StringLength(55)]
        [DisplayName("Nazwa")]
        public string Name { get; set; }

        [Required]
        [DisplayName("Sortowanie")]
        public int Sort { get; set; }

        public virtual ICollection<Forum> Forums { get; set; }
    }
}