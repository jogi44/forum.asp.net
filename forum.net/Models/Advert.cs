﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace forum.net.Models
{
    public class Advert : BaseEntity
    {
        public int AdvertId { get; set; }

        [Required]
        [StringLength(35)]
        [DisplayName("Tytuł")]
        public string Title { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayName("Treść")]
        public string Body { get; set; }

        [ForeignKey("CreatedBy")]
        public string CreatedById { get; set; }

        [ForeignKey("UpdatedBy")]
        public string UpdatedById { get; set; }

        public ApplicationUser CreatedBy { get; set; }
        public ApplicationUser UpdatedBy { get; set; }
    }
}