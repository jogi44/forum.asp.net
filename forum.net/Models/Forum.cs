﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace forum.net.Models
{
    public class Forum : BaseEntity
    {
        public int ForumId { get; set; }

        [Required]
        [StringLength(15)]
        [DisplayName("Nazwa")]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayName("Opis")]
        public string Description { get; set; }

        [Required]
        [DisplayName("Sortowanie")]
        public int Sort { get; set; }

        public int CategoryId { get; set; }

        [DisplayName("Kategoria")]
        public Category Category { get; set; }

        public virtual  ICollection<Thread> Threads { get; set; }
    }

    public class CreateForumViewModel
    {
        [Required]
        [StringLength(15)]
        [Display(Name = "Nazwa")]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "Opis")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Sortowanie")]
        public int Sort { get; set; }

        [Required]
        [Display(Name = "Kategoria")]
        public int CategoryId { get; set; }

        public IEnumerable<SelectListItem> Categories { get; set; }
    }

    public class EditForumViewModel
    {
        [Required]
        [StringLength(15)]
        [Display(Name = "Nazwa")]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "Opis")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Sortowanie")]
        public int Sort { get; set; }

        [Required]
        [Display(Name = "Kategoria")]
        public int CategoryId { get; set; }

        public Forum forum { get; set; }
        public DateTime CreatedAt { get; set; }

        public IEnumerable<SelectListItem> Categories { get; set; }
    }
}