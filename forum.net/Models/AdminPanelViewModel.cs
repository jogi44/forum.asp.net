﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace forum.net.Models
{
    public class AdminPanelViewModel
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ICollection<Category> Categories => db.Categories.ToList();
        public ICollection<Forum> Forums => db.Fora.ToList();
        public ICollection<ApplicationUser> Users => db.Users.ToList();
        public ICollection<Threadtype> Threadtypes => db.Threadtypes.ToList();
        public ICollection<Advert> Adverts => db.Adverts.ToList();

        public Advert Advert { get; set; }
        public Category Category { get; set; }
        public Forum Forum { get; set; }
        public Threadtype Threadtype { get; set; }
    }
}