﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(forum.net.Startup))]
namespace forum.net
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
